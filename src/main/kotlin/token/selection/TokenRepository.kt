package token.selection

class TokenRepository(vararg tokens: Token) {

    private val tokens: MutableMap<Token, Player?> = mutableMapOf()

    init {
        tokens.forEach { token -> this.tokens[token] = null }
    }

    fun availableTokens(): Set<Token> {
        synchronized(this) {
            return tokens.entries.fold(mutableSetOf()) { availableTokens, entry ->
                if (entry.value == null) {
                    availableTokens.add(entry.key)
                }
                availableTokens
            }
        }
    }

    fun selectedToken(player: Player): Token? {
        synchronized(this) {
            return tokens.entries.find { entry -> player == entry.value }?.key
        }
    }

    fun select(token: Token, player: Player) {
        synchronized(this) {
            assertValidToken(token)

            when (tokens[token]) {
                null -> tokens[token] = player
                else -> throw TokenAlreadySelectedException()
            }
        }
    }

    fun deselect(token: Token, player: Player) {
        synchronized(this) {
            assertValidToken(token)

            when {
                tokens[token] == player -> tokens[token] = null
                tokens[token] == null -> throw TokenNotYetSelectedException()
                else -> throw NotAllowedOperationException()
            }

        }
    }

    fun changeToken(newToken: Token, player: Player) {
        synchronized(this) {
            assertValidToken(newToken)

            val currentToken: Token =
                selectedToken(player) ?: throw NoPreviouslySelectedTokenException()

            if (currentToken != newToken) {
                when {
                    tokens[newToken] == null -> {
                        tokens[currentToken] = null
                        tokens[newToken] = player
                    }
                    else -> throw NotAllowedOperationException()
                }
            }
        }
    }

    private fun assertValidToken(token: Token) {
        if (!tokens.containsKey(token)) {
            throw TokenNotValidException()
        }
    }

}