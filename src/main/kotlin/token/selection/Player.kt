package token.selection

class Player(val id: String) {

    override fun equals(other: Any?): Boolean {
        return other is Player && id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}