package token.selection

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class TokenRepositoryTest {

    private lateinit var tokenRepository: TokenRepository

    @BeforeEach
    fun setUp() {
        tokenRepository = TokenRepository(Token.RED, Token.GREEN, Token.BLUE)
    }

    @Test
    fun whenNoTokenHasBeenSelectedThenAllTokensShouldBeAvailable() {
        val availableTokens = tokenRepository.availableTokens()

        assertEquals(3, availableTokens.size)
    }

    @Test
    fun whenTokenHasBeenSelectedThenItShouldNotBeAvailable() {
        tokenRepository.select(Token.GREEN, Player("foo"))

        val availableTokens = tokenRepository.availableTokens()

        assertFalse(availableTokens.contains(Token.GREEN))
    }

    @Test
    fun whenPlayerHasSelectedTokenThenItShouldBeMarkedAsSelected() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)

        assertEquals(Token.GREEN, tokenRepository.selectedToken(player))
    }

    @Test
    fun whenPlayerHasNotSelectedTokenThenItShouldNotBeMarkedAsSelected() {
        assertNull(tokenRepository.selectedToken(Player("foo")))
    }

    @Test
    fun whenNonExistingTokenIsSelectedThenItShouldThrowException() {
        assertThrows<TokenNotValidException> {
            tokenRepository.select(Token.YELLOW, Player("foo"))
        }
    }

    @Test
    fun whenAlreadySelectedTokenIsSelectedThenItShouldThrowException() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)

        assertThrows<TokenAlreadySelectedException> {
            tokenRepository.select(Token.GREEN, player)
        }
    }

    @Test
    fun whenSelectedTokenHasBeenDeselectedThenItShouldBeAvailable() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)
        tokenRepository.deselect(Token.GREEN, player)

        assertTrue(tokenRepository.availableTokens().contains(Token.GREEN))
    }

    @Test
    fun whenSelectedTokenIsDeselectedByOtherPlayerThenItShouldThrowException() {
        val player1 = Player("foo")
        val player2 = Player("bar")

        tokenRepository.select(Token.GREEN, player1)

        assertThrows<NotAllowedOperationException> {
            tokenRepository.deselect(Token.GREEN, player2)
        }
    }

    @Test
    fun whenNonSelectedTokenIsDeselectedThenItShouldThrowException() {
        assertThrows<TokenNotYetSelectedException> {
            tokenRepository.deselect(Token.GREEN, Player("foo"))
        }
    }

    @Test
    fun whenNonExistingTokenIsDeselectedThenItShouldThrowException() {
        assertThrows<TokenNotValidException> {
            tokenRepository.deselect(Token.YELLOW, Player("foo"))
        }
    }

    @Test
    fun whenChangeTokenThenTheNewTokenShouldBelongToPlayer() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)
        tokenRepository.changeToken(Token.BLUE, player)

        assertEquals(Token.BLUE, tokenRepository.selectedToken(player))
    }

    @Test
    fun whenChangeTokenThenTheOldTokenShouldBeAvailable() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)
        tokenRepository.changeToken(Token.BLUE, player)

        assertTrue(tokenRepository.availableTokens().contains(Token.GREEN))
    }

    @Test
    fun whenChangeTokenThenTheNewTokenShouldNotBeAvailable() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)
        tokenRepository.changeToken(Token.BLUE, player)

        assertFalse(tokenRepository.availableTokens().contains(Token.BLUE))
    }

    @Test
    fun whenChangeTokenIsRequestedOnSameTokenThenItShouldIgnoreTheChange() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)
        tokenRepository.changeToken(Token.GREEN, player)

        assertFalse(tokenRepository.availableTokens().contains(Token.GREEN))
    }

    @Test
    fun whenChangeTokenIsRequestedButNoPreviouslySelectedTokenThenItShouldThrowException() {
        assertThrows<NoPreviouslySelectedTokenException> {
            tokenRepository.changeToken(Token.GREEN, Player("foo"))
        }
    }

    @Test
    fun whenChangeTokenIsRequestedOnNonExistingTokenThenItShouldThrowException() {
        val player = Player("foo")

        tokenRepository.select(Token.GREEN, player)

        assertThrows<TokenNotValidException> {
            tokenRepository.changeToken(Token.YELLOW, player)
        }
    }

    @Test
    fun whenChangeTokenIsRequestedOnAlreadySelectedTokenThenItShouldThrowException() {
        val player1 = Player("foo")
        val player2 = Player("bar")

        tokenRepository.select(Token.GREEN, player1)
        tokenRepository.select(Token.BLUE, player2)

        assertThrows<NotAllowedOperationException> {
            tokenRepository.changeToken(Token.BLUE, player1)
        }
    }

}