package token.selection

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class PlayerTest {

    @Test
    fun whenComparedWithSamePlayerInstanceThenItShouldCompareAsEqual() {
        val player = Player("foo")

        assertEquals(player, player)
    }

    @Test
    fun whenComparedWithOtherPlayerInstanceWithSameIdThenItShouldCompareAsEqual() {
        val player = Player("foo")
        val otherPlayer = Player("foo")

        assertEquals(player, otherPlayer)
    }

    @Test
    fun whenComparedWithOtherPlayerInstanceWithDifferentIdThenItShouldCompareAsNotEqual() {
        val player = Player("foo")
        val otherPlayer = Player("bar")

        assertNotEquals(player, otherPlayer)
    }

}